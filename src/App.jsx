import { useEffect, useState } from 'react';
import './App.css';
import {firebase} from './firebase'

function App() {

  const [procesos, serProcesos] = useState([])

  const obtenerDatos = async () => {
    try {
      const db = firebase.firestore()
      const data = await db.collection('procesos').get()
      const arrayData = data.docs.map(doc => ({id:doc.id, ...doc.data()}))
      serProcesos(arrayData)
    }
    catch(error) {
      console.log(error)
    }
  }

  useEffect(() => {
    obtenerDatos()
  }, [])

  return (
    <div className="container mt-5">
      <div className='col-8'>
          <h4 className='text-center'>Listado de Procesos</h4>
          <ul className='list-group'>
            {
              procesos.map( item => (
                <li className='list-group-item' key={item.id}>
                  <span className='lead'>{item.nombre}</span>
                </li>
              ))
            }
          </ul>
        </div>
    </div>
  );
}

export default App;
