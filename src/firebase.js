import firebase from "firebase/app";
import 'firebase/firestore'


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB0_zrnH2DYNDw2ALuxqv8vQEvmgThejpM",
  authDomain: "taller-final-react.firebaseapp.com",
  databaseURL: "https://taller-final-react.firebaseio.com",
  projectId: "taller-final-react",
  storageBucket: "taller-final-react.appspot.com",
  messagingSenderId: "165070770748",
  appId: "1:165070770748:web:962cf7e86717815e2e6939"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export {firebase}